package de.friedenhagen.snakeyamlanchors;

import java.util.Map;

public class Stuff {
    enum GitlabAccessLevel {
        OWNER,
        MAINTAINER,
        DEVELOPER,
        REPORTER,
        GUEST;
    }

    enum ArtifactoryPermission {
        READ_ONLY,
        READ_WRITE_ANNOTATE,
        READ_WRITE_DELETE_ANNOTATE;
    }

    public static class PatternPermission {
        public Map<GitlabAccessLevel, ArtifactoryPermission> permission_map;
    }

    public Map<String, PatternPermission> pattern_permissions;

}
