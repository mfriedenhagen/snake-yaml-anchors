package de.friedenhagen.snakeyamlanchors;

import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class StuffTest {

    private Yaml yaml;
    private Stuff subjectUnderTest;

    @Before
    public void loadYaml() throws FileNotFoundException {
        Representer representer = new Representer();
        representer.getPropertyUtils().setSkipMissingProperties(true);
        yaml = new Yaml(new Constructor(Stuff.class), representer);
        subjectUnderTest = yaml.loadAs(new FileReader("permissions.yml"), Stuff.class);
    }

    @Test
    public void direct_references_are_deserialized_properly() {
        final Map<Stuff.GitlabAccessLevel, Stuff.ArtifactoryPermission> permission_map = subjectUnderTest.pattern_permissions.get("blob-perm").permission_map;
        assertThat(permission_map.get(Stuff.GitlabAccessLevel.OWNER), is(Stuff.ArtifactoryPermission.READ_WRITE_DELETE_ANNOTATE));
        assertThat(permission_map.get(Stuff.GitlabAccessLevel.MAINTAINER), is(Stuff.ArtifactoryPermission.READ_WRITE_ANNOTATE));
    }

    @Test
    public void anchor_references_are_not_deserialized_properly() {
        final Map<Stuff.GitlabAccessLevel, Stuff.ArtifactoryPermission> permission_map = subjectUnderTest.pattern_permissions.get("blob-dev").permission_map;
        assertThat(permission_map.get(Stuff.GitlabAccessLevel.MAINTAINER), is(Stuff.ArtifactoryPermission.READ_WRITE_DELETE_ANNOTATE));
    }

    @Test
    public void anchor_references_are_not_deserialized_properly_but_as_string() {
        final Map<Stuff.GitlabAccessLevel, Stuff.ArtifactoryPermission> permission_map = subjectUnderTest.pattern_permissions.get("blob-dev").permission_map;
        assertThat(permission_map.get(Stuff.GitlabAccessLevel.MAINTAINER.toString()), is(Stuff.ArtifactoryPermission.READ_WRITE_DELETE_ANNOTATE.toString()));
    }
}